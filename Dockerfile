# This file is a template, and might need editing before it works on your project.
FROM java:8-jre

COPY assembly/target/ /app/
WORKDIR /app/

EXPOSE 8088
CMD ["java", "-jar", "main.jar"]